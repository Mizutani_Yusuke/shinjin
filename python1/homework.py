#1問目
print("1.")
for i in range(1, 31):
    print(i)
#2問目
print("2.")
for i in range(2, 31,2):
    print(i)
#３問目
print("3.")
var = 0
i = 0
while var < 31:
    i = var + i
    var += 1
print(i)
#4問目
print("4.")
n = 1
m = 1
while m < 10:
    n = n * m
    m += 1
print(n)
#5問目
print("5.")
m = 1
while m < 10:
    n = 1
    while n < 9:
        print(n * m)
        n += 1
    print(m * n)
    m += 1
#6問目
print("6.")
m = 1
while m < 31:
    if m % 3 == 0 and not m % 5 == 0:
        print("Fizz")
    elif m % 5 == 0  and not m % 3 == 0:
        print("Buzz")
    elif m % 3 == 0 and m % 5 == 0:
        print("FizzBuzz")
    else:
        print(m)
    m += 1
#7問目
print("7.")
m = 2
while m <= 1000:
    n = 1
    l = 0
    while n <= m:
        if m % n == 0:
            l += 1
        else:
            l += 0
        n += 1
    if l == 2:
        print(m)
    else:
        m += 0
    m += 1
